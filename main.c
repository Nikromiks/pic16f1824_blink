/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#endif

#include "system.h"        /* System funct/params, like osc/peripheral config */
#include "main.h"
#include "appBlink.h"          /* User funct/params, such as InitApp */
#include "appAlarm.h"
#include "time.h"
#include "eeprom.h"
#include "appThanksProg.h"
/******************************************************************************/
/* User Global Variable Declaration                                           */
/******************************************************************************/
//type connect, LOW or HIGH
volatile typeEnum type = LOW;

unsigned int eeprom_timePeriod = 0xFFFF;
unsigned int eeprom_timePulse = 0xFFFF;
unsigned char eeprom_countPulse = 0xFF;
unsigned char eeprom_countAlarm = 0xFF;

//type blinker in now, LEFT or RIGHT
volatile blinkEnum leftRigth;
/******************************************************************************/
/* Main Program
 * http://diymicro.ru/pic-laboratoriya/comment-page-1#comment-5718      */
/******************************************************************************/
void initApp(void) {
    //pull off
    ANSELA = 0;
    nWPUEN = 0;
    WPUA = 0;
    WPUA3 = 1;
    WPUA5 = 1;

    ANSELC = 0;
    WPUC = 9;
    //in
    TRISA3 = 1; //IN_THANKS_PROG
    TRISC3 = 1; //rigth blinker
    TRISC0 = 1; //left blinker
    TRISA4 = 1; //IN_NULL

    TRISA5 = 1; //IN_ALARM
    //TRISA2 = 1;//IN_BREAKE_STOP
    //TRISA4 = 1;//IN_VOLT_OR_SPEED
    //out
    TRISC1 = 0; //OUT_LEFT_HIGH_BLINKER
    TRISC4 = 0; //OUT_RIGHT_HIGH_BLINKER
    TRISC5 = 0; //OUT_RIGHT_LOW_BLINKER
    TRISC2 = 0; //OUT_LEFT_LOW_BLINKER

    TRISA2 = 0; //OUT_ALARM
    //TRISA1 = 0; //OUT_HEAD_LAMP
    //TRISA0 = 0; //OUT_MARKER_LAMP

    lowOut();
    OUT_MARKER_LAMP(0);
    OUT_HEAD_LAMP(0);

    TMR2 = 0;
    T2CON = 0b00000111; // 0.001c
    PR2 = 125;

    GIE = 1;
    PEIE = 1;
    TMR2IE = 1;

    loadFromEEPROM();
}

void main(void)
{
    ConfigureOscillator();

    initApp();
    //initAlarm();
     while (1) {
        appThanksProg();
        appBlinkWork();
        //appAlarm();
    }
}

