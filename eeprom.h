/* 
 * File:   eeprom.h
 * Author: User
 *
 * Created on 19 ?????? 2013 ?., 21:52
 */

#ifndef EEPROM_H
#define	EEPROM_H

#ifdef	__cplusplus
extern "C" {
#endif

#define ADR_HI_TIME_PERIOD 0x00
#define ADR_LO_TIME_PERIOD 0x01
#define ADR_HI_TIME_PULSE 0x02
#define ADR_LO_TIME_PULSE 0x03
#define ADR_COUNT_PULSE 0x04
    
#define ADR_TIME_ALARM 0x06

void saveToEEPROM(void);
void loadFromEEPROM(void);

#ifdef	__cplusplus
}
#endif

#endif	/* EEPROM_H */

