/* 
 * File:   time.h
 * Author: User
 *
 * Created on 9 ???? 2013 ?., 1:06
 */

#ifndef TIME_H
#define	TIME_H

#ifdef	__cplusplus
extern "C" {
#endif
    
unsigned long int getTime();
void delayMs(int ms);

extern volatile unsigned long int timeCount;
#ifdef	__cplusplus
}
#endif

#endif	/* TIME_H */

