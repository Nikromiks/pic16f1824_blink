/******************************************************************************/
/* User IO PORTS                                                              */
/******************************************************************************/
#define OUT_LEFT_LOW_BLINKER(x) RC2 = x;
#define OUT_LEFT_HIGH_BLINKER(x) RC1 = x;
#define OUT_RIGHT_LOW_BLINKER(x) RC5 = x;
#define OUT_RIGHT_HIGH_BLINKER(x) RC4 = x;

#define IN_NULL RA4
#define IN_THANKS_PROG RA3
#define IN_RIGHT_BLINKER !RC3
#define IN_LEFT_BLINKER !RC0

#define OUT_MARKER_LAMP(x) RA0 = x;
#define OUT_HEAD_LAMP(x) RA1 = x;

#define IN_ALARM RA5
#define OUT_ALARM_TIMER(x) RA2 = x;

#define IN_BREAKE_STOP RA2 //
#define IN_VOLT_OR_SPEED RA4

/*
 * State blink app
 */
typedef enum{
     WAIT_START, START, WAIT_PROG_HIGH, WAIT_PROG_LOW,
            PROG_HIGH, PROG_LOW,
            WORK_LOW, WORK_HIGH, 
            WORK_HIGH_BLINK, WORK_BLINK_LOW,
            TRIG_LEFT,TRIG_RIGHT
}stateEnum;

/*
State sensor app
 */
typedef enum{
    SENSOR_START, SENSOR_WAIT_CHANGE, SENSOR_TRIG
}stateSensorEnum;

/*
State lamp app
 */
typedef enum{
    IDLE, WAIT_ON, WAIT_OFF_HEAD, WAIT_OFF_MARKER
}stateLampEnum;




void appBlinkWork();

void startProgramming();

void lowOut();
void pulseStrob(int pause);
stateEnum getStateBlink();
void setStateBlink(stateEnum newState);