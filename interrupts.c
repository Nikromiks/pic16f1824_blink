/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#endif

#include "time.h"

/******************************************************************************/
/* Interrupt Routines                                                         */
/******************************************************************************/

void interrupt isr(void)
{
    if(TMR2IF){
        GIE = 0;
        TMR2 = 0;
        T2CON = 0b00000111; //???????? ?????? 0.001c
        TMR2IF = 0;  //????? ?????
        timeCount++;
        GIE = 1;
    }
}



