#if defined(__XC)
#include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
#include <htc.h>        /* HiTech General Include File */
#endif

#include "eeprom.h"
#include "main.h"
#include "time.h"
#include "appAlarm.h"
#include <eeprom_routines.h>

#define TIME_DELAY_WRITE_EEPROM 20

void saveToEEPROM(void) {
    unsigned char data = (unsigned char) (eeprom_timePeriod >> 8);
    EEPROM_WRITE(ADR_HI_TIME_PERIOD, data);
    delayMs(TIME_DELAY_WRITE_EEPROM);

    data = (unsigned char) (eeprom_timePeriod & 0x00FF);
    EEPROM_WRITE(ADR_LO_TIME_PERIOD,data);
    delayMs(TIME_DELAY_WRITE_EEPROM);

    data =(unsigned char) (eeprom_timePulse >> 8);
    EEPROM_WRITE(ADR_HI_TIME_PULSE,data);
    delayMs(TIME_DELAY_WRITE_EEPROM);

    data = (unsigned char) (eeprom_timePulse & 0x00FF);
    EEPROM_WRITE(ADR_LO_TIME_PULSE,data);
    delayMs(TIME_DELAY_WRITE_EEPROM);
    
    EEPROM_WRITE(COUNT_PULSE, eeprom_countPulse);
    delayMs(TIME_DELAY_WRITE_EEPROM);

    EEPROM_WRITE(ADR_TIME_ALARM,eeprom_countAlarm);
    delayMs(TIME_DELAY_WRITE_EEPROM);

}

void loadFromEEPROM(void) {
    eeprom_timePeriod = (unsigned int) (EEPROM_READ(ADR_LO_TIME_PERIOD) + (unsigned int) (EEPROM_READ(ADR_HI_TIME_PERIOD) << 8));
    eeprom_timePulse = (unsigned int) (EEPROM_READ(ADR_LO_TIME_PULSE) + (unsigned int) (EEPROM_READ(ADR_HI_TIME_PULSE) << 8));
    eeprom_countPulse = EEPROM_READ(COUNT_PULSE);
    eeprom_countAlarm = EEPROM_READ(ADR_TIME_ALARM);
    
    if(eeprom_timePeriod == 0xFFFF && eeprom_timePulse == 0xFFFF && eeprom_countPulse == 0xFF){
        eeprom_timePeriod = TIME_PERIOD;
        eeprom_timePulse = TIME_PULSE;
        eeprom_countPulse = COUNT_PULSE;
        eeprom_countAlarm = TIME_ALARM_TIMER;
        saveToEEPROM();
    }
}
