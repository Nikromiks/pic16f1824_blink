/* 
 * File:   main.h
 * Author: User
 *
 * Created on 22 ??????? 2013 ?., 19:35
 */

#ifndef MAIN_H
#define	MAIN_H

#ifdef	__cplusplus
extern "C" {
#endif

/******************************************************************************/
/* User config defines                                                        */
/******************************************************************************/
#define TIME_WAIT_START 200 //0.2c
#define TIME_WAIT_START_PROG 4000
#define TIME_WAIT_PWM 50
#define TIME_WAIT_BLINK 200
#define TIME_WAIT_LOW_LEVEL 50
#define TIME_PROG_LIMIT 60000
#define TIME_PULSE_IN_PROG 1000
#define TIME_PULSE_OUT_PROG 300

/*
 * Settings blink
 */
#define TIME_PERIOD 6000
#define TIME_PULSE 1000
#define COUNT_PULSE 4
#define TIME_THANKS_PULSE 800
#define TIME_THANKS 1000
#define COUNT_THANKS_PULSE 2
/*
 * Settings lamp app
 */
#define TIME_WAIT_ON 3000
#define TIME_WAIT_OFF_HEAD_LAMP 180000
#define TIME_WAIT_OFF_MARKER_LAMP 15000

/*
 Type sensor
 */
typedef enum{
    VOLTAGE,SPEED
}typeSensor;
/*
 Type blinker
 */
typedef enum{
    HIGH, LOW
}typeEnum;

/*
 Side blinker
 */
typedef enum{
    RIGTH, LEFT
}blinkEnum;


extern unsigned int eeprom_timePeriod;
extern unsigned int eeprom_timePulse;
extern unsigned char eeprom_countPulse;
extern unsigned char eeprom_countAlarm;

extern volatile typeEnum type;
extern volatile blinkEnum leftRigth;



#ifdef	__cplusplus
}
#endif

#endif	/* MAIN_H */

